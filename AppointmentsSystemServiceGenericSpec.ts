

namespace Aversi
{

    /// Methods

    /**
     * აბრუნებს ფილიალების სიას
     */
    declare function GetBranches():GetBranchesResult

    /**
     * აბრუნებს განყოფილებების სიას
     * 
     * @param branchId ფილიალის იდენტიფიკატორი
     */
    declare function GetDepartmentsInBranch(branchId:number):GetDepartmentsInBrachResult

    /**
     * აბრუნებს განყოფილებაში შემავალი პროცედურების/კვლევების სიას
     * 
     * @param departmentId განყოფილების იდენტიფიკატორი
     */
    declare function GetServicesInDepartment(departmentId:number):GetServicesInDepartmentResult

    /**
     * აბრუნებს ექიმების სიას რომლებიც აწარმოებენ მითითებულ კვლევა/პროცედურას
     * 
     * @param serviceId პროცედურის/კვლევის იდენტიფიკატორი (პროცედურა/კვლევა არის განყოფილების შვილი)
     */
    declare function GetDoctorsListInService(serviceId:number):GetDoctorListResult

    /**
     * აბრუნებს ექიმის კალენდარს სადაც არის ასახული ხელმისაწვოდმი და დაკავებული საათები
     * 
     * @param doctorId ექიმის იდენტიფიკატორი
     */
    declare function GetCalendarForDoctor(doctorId:number):GetCalendarForDoctorResult

    /**
     * მითითებული ექიმის საათში ჩაწერა
     * 
     * @param doctorId ექიმის იდენტიფიკატორი
     * @param date თარიღი
     * @param hourFrom საათი საიდან
     * @param hourTo საათი სადამდე
     */
    declare function ReserveDoctorHour(patientData:{phoneNumber:String, firstName:string, lastName:string, pID:string}, 
    doctorId:number, date:Date, hourFrom:number, hourTo:number):ReserveDoctorHourResult

    /**
     * რეზერვაციის დამოწმება (ოპერატორის მიერ)
     * 
     * @param reservationId 
     */
    declare function ApproveReservation(reservationId:number):ApproveReservationResult

    /**
     * რეზერვაციის გაუქმება (ოპერატორის მიერ)
     * @param reservationId 
     */
    declare function CancelReservation(reservationId:number):CancelReservationResult

    /// Types

    interface CancelReservationResult extends FunctionResult
    {}

    interface ApproveReservationResult extends FunctionResult
    {}

    interface Branch
    {
        id:number
        name:string
    }

    interface GetBranchesResult extends FunctionResult
    {
        branches:Array<Branch>
    }

    interface ReserveDoctorHourResult extends FunctionResult
    {
        reservationId:number
    }    

    interface GetServicesInDepartmentResult extends FunctionResult
    {
        serivices:Array<ServiceInDeparmtment>
    }

    interface ServiceInDeparmtment
    {
        id:number
        name:string        
    }

    interface GetDepartmentsInBrachResult extends FunctionResult
    {
        departments:Array<Department>
    }

    interface GetCalendarForDoctorResult extends FunctionResult
    {
        days:Array<DoctorCalendarDay>
    }

    interface DoctorCalendarDay
    {
        date:Date
        hours:Array<DoctorHour>
    }

    interface DoctorHour
    {
        hour:number // საათის ნომერი, 0,1,2,3...15,16,..24
        isAvailable:boolean
    }

    interface GetDoctorListResult extends FunctionResult
    {
        doctors:Array<Doctor>
    }

    interface Doctor
    {
        id:number
        name:string
    }

    /**
     * სამედიცინო სამუშაო განყოფილება: ულტრასონოგრაფიული კველვა, რენდგენი
     */
    interface Department
    {
        id:number
        name:string
    }

    interface FunctionResult
    {
        errorCode?:number
    }

    enum ErrorCode
    {
        HourNotAvaileble = 10, //ექიმის დრო არ არის ხელმისაწვოდმი

    }
}